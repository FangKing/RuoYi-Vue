package com.ruoyi.framework.web.service;

import com.ruoyi.common.exception.user.UserNotExistsException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.EbUser;
import com.ruoyi.system.service.IEbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.domain.model.RegisterBody;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.exception.user.CaptchaException;
import com.ruoyi.common.exception.user.CaptchaExpireException;
import com.ruoyi.common.utils.MessageUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.manager.AsyncManager;
import com.ruoyi.framework.manager.factory.AsyncFactory;
import com.ruoyi.system.service.ISysConfigService;
import com.ruoyi.system.service.ISysUserService;

import javax.security.auth.Subject;

/**
 * 注册校验方法
 * 
 * @author ruoyi
 */
@Component
public class SysRegisterService
{
    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private IEbUserService ebUserService;

    /**
     * 注册
     */
    public String register(RegisterBody registerBody)
    {
        String msg = "", userName = registerBody.getUserName(), password = registerBody.getPassword();
        SysUser sysUser = new SysUser();
        sysUser.setUserName(userName);

        // 验证码开关
        boolean captchaEnabled = configService.selectCaptchaEnabled();
        if (captchaEnabled)
        {
            validateCaptcha(userName, registerBody.getCode(), registerBody.getUuid());
        }

        if (StringUtils.isEmpty(userName))
        {
            msg = "用户名不能为空";
        }
        else if (StringUtils.isEmpty(password))
        {
            msg = "用户密码不能为空";
        }
        else if (userName.length() < UserConstants.USERNAME_MIN_LENGTH
                || userName.length() > UserConstants.USERNAME_MAX_LENGTH)
        {
            msg = "账户长度必须在2到20个字符之间";
        }
        else if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
        {
            msg = "密码长度必须在5到20个字符之间";
        }
        else if (!userService.checkUserNameUnique(sysUser))
        {
            msg = "保存用户'" + userName + "'失败，注册账号已存在";
        }
        else
        {
            sysUser.setNickName(userName);
            sysUser.setPassword(SecurityUtils.encryptPassword(password));
            boolean regFlag = userService.registerUser(sysUser);
            if (!regFlag)
            {
                msg = "注册失败,请联系系统管理人员";
            }
            else
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(userName, Constants.REGISTER, MessageUtils.message("user.register.success")));
            }
        }
        return msg;
    }

    public String wxRegister(RegisterBody registerBody) {
        String msg = "";

        String openId = registerBody.getOpenId();
        if (StringUtils.isEmpty(openId))
        {
            msg = "openId不能为空";
        } else if (StringUtils.isAnyEmpty(registerBody.getPhonenumber(), registerBody.getUserName(), registerBody.getCompany(), registerBody.getCode())) {
            msg = "手机号，姓名，公司和验证码都不能为空";
        } else {
            // 用户验证
            String phone = registerBody.getPhonenumber();
            String code = registerBody.getCode();

            String codeKey = CacheConstants.LOGIN_CODE_KEY + phone;

            String cacheCode = redisCache.getCacheObject(codeKey);

            if (cacheCode == null)
            {
                throw new CaptchaExpireException();
            }
            if (!code.equalsIgnoreCase(cacheCode))
            {
                throw new CaptchaException();
            }

            redisCache.deleteObject(codeKey);

            EbUser ebUser = ebUserService.selectEbUserByPhone(phone);

            if (ebUser == null || !StringUtils.equals(registerBody.getUserName(), ebUser.getUserName())
                    || !StringUtils.equals(registerBody.getCompany(), ebUser.getBranchCompany())) {
                throw new UserNotExistsException("ebuser.not.exists");
            }

            SysUser user = new SysUser();

            // 此处从微信获取用户相关信息并保存
            user.setNickName(registerBody.getUserName());
            user.setUserName(registerBody.getPhonenumber());
            user.setCompany(ebUser.getCompany());
            user.setPhonenumber(registerBody.getPhonenumber());
            user.setOpenId(openId);
            user.setPassword(SecurityUtils.encryptPassword("123456"));

            boolean regFlag = userService.registerUser(user);
            if (!regFlag)
            {
                msg = "注册失败,请联系系统管理人员";
            }
            else
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(registerBody.getUserName(), Constants.REGISTER, MessageUtils.message("user.register.success")));
            }

            if (StringUtils.isEmpty(msg)) {
                if (StringUtils.equals(ebUser.getRole(), "分发人")) {
                    userService.insertUserAuth(user.getUserId(), new Long[]{100l});
                } else {
                    userService.insertUserAuth(user.getUserId(), new Long[]{101l});
                }
            }
        }
        return msg;
    }

    /**
     * 校验验证码
     * 
     * @param username 用户名
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid)
    {
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + StringUtils.nvl(uuid, "");
        String captcha = redisCache.getCacheObject(verifyKey);
        redisCache.deleteObject(verifyKey);
        if (captcha == null)
        {
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha))
        {
            throw new CaptchaException();
        }
    }
}
