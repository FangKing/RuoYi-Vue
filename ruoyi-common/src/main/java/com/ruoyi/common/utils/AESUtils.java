package com.ruoyi.common.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AESUtils {

    /**
     * Cipher Map
     */
    private static final Map<String, Cipher> CIPHERMAP_MAP = new ConcurrentHashMap<>();

    private static final String DEFAULT_CHARSET = "utf-8";
    private static final String ALGORITHM_NAME = "AES";

    /**
     * base64 and cyberark 算法
     */
    public static final String PKCS5CBC_CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";

    /**
     * 标准AES IV
     */
    private static final String STANDARD_AES_IV = "1234567890123456";

    /**
     * 根据加密类型获取加密器
     *
     * @param mode
     * @param key
     * @param charset
     * @return
     * @throws Exception
     */
    private static Cipher getCipher(int mode, String key, String charset) throws Exception {
        String cipherKey = mode + "_" + "_1"; //mode + "_" + key + "_1"
        Cipher cipher = CIPHERMAP_MAP.get(cipherKey);
        if (cipher != null) {
            return cipher;
        }

        cipher = getCipherBaseOnIv(mode, key, charset, STANDARD_AES_IV);

        CIPHERMAP_MAP.put(cipherKey, cipher);
        return cipher;
    }

    /**
     * 根据Iv生成加密器
     *
     * @param mode
     * @param key
     * @param charset
     * @param iv
     * @return
     */
    private static Cipher getCipherBaseOnIv(int mode, String key, String charset, String iv) throws Exception {
        Cipher cipher = Cipher.getInstance(PKCS5CBC_CIPHER_ALGORITHM);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key.getBytes(charset), ALGORITHM_NAME);
        cipher.init(mode, secretKeySpec, new IvParameterSpec(iv.getBytes(charset)));
        return cipher;
    }

    /**
     * 加密
     *
     * @param a
     * @param key
     * @param charset
     * @return
     * @throws Exception
     */
    private static String encrypt(String a, String key, String charset) throws Exception {
        Cipher cipher = getCipher(Cipher.ENCRYPT_MODE, key, charset);
        byte[] resultByte = cipher.doFinal(a.getBytes(charset));

        return new String(Base64.encodeBase64(resultByte), charset);
    }

    /**
     * 默认UTF-8加密
     *
     * @param source
     * @param key
     * @return
     * @throws Exception
     */
    public static String encrypt(String source, String key) throws Exception {
        return encrypt(source, key, DEFAULT_CHARSET);
    }

    /**
     * 解密
     *
     * @param source
     * @param key
     * @param charset
     * @return
     * @throws Exception
     */
    private static String decrypt(String source, String key, String charset) throws Exception {
        Cipher cipher = getCipher(Cipher.DECRYPT_MODE, key, charset);
        byte[] inputByte = Base64.decodeBase64(source.getBytes(charset));

        byte[] resultByte = cipher.doFinal(inputByte);
        return new String(resultByte, charset);
    }

    /**
     * 默认UTF-8解密
     *
     * @param source
     * @param key
     * @return
     * @throws Exception
     */
    public static String decrypt(String source, String key) throws Exception {
        return decrypt(source, key, DEFAULT_CHARSET);
    }

    public static void main(String[] args) throws Exception {
        String data = "{\"sourceId\": \"EZRL2ENMJ69R4375YB03\", \"insuredName\": \"张三\", \"riskName\": \"一种保险\", \"policyNo\": \"ABCDEFG\", \"reportPerson\": \"李四\", \"reportPersonPhone\": \"15800817461\",\"reportTime\":\"2023-12-20 12:44:55\",\"riskTime\":\"2023-10-20 12:44:55\", \"riskAddress\": \"北京市昌平区\"}";
        String key = "4tqrbl0yg3fui0py";

        System.out.println(data);
        //加密
        String content = encrypt(data, key);
        System.out.println(content);
        //解密
        System.out.println(decrypt(content, key));
    }
}

