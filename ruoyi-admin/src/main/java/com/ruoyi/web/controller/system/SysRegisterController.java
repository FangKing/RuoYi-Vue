package com.ruoyi.web.controller.system;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import com.ruoyi.common.constant.CacheConstants;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.exception.user.UserNotExistsException;
import com.ruoyi.framework.web.service.SysLoginService;
import com.ruoyi.system.domain.EbUser;
import com.ruoyi.system.service.IEbUserService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.model.RegisterBody;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.service.SysRegisterService;
import com.ruoyi.system.service.ISysConfigService;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 注册验证
 * 
 * @author ruoyi
 */
@CrossOrigin
@RestController
public class SysRegisterController extends BaseController
{
    @Autowired
    private SysRegisterService registerService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private ISysUserService userService;

    @Autowired
    private SysLoginService loginService;

    @Autowired
    private IEbUserService ebUserService;

    @Autowired
    private RedisCache redisCache;

    @PostMapping("/register")
    public AjaxResult register(@RequestBody RegisterBody user)
    {
        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser"))))
        {
            return error("当前系统没有开启注册功能！");
        }
        String msg = StringUtils.EMPTY;

        if (StringUtils.isEmpty(user.getOpenId())) {
            msg = registerService.register(user);
        } else {
            SysUser sysUser = userService.selectUserByIdOpenid(user.getOpenId());

            if (sysUser == null) {
                msg = registerService.wxRegister(user);
            }

            if (StringUtils.isEmpty(msg)){
                // 模拟登录
                String token = loginService.wxLogin(user.getOpenId());
                return success().put(Constants.TOKEN, token);
            }
        }

        return StringUtils.isEmpty(msg) ? success() : error(msg);
    }

    @GetMapping("/sendRgCode")
    public AjaxResult sendRgCode(String phone){
        if (StringUtils.isEmpty(phone)) {
            return error("手机号不能为空");
        }

        if (!Validator.isMobile(phone)) {
            return error("手机号不合法");
        }

        EbUser ebUser = ebUserService.selectEbUserByPhone(phone);

        if (ebUser == null) {
            throw new UserNotExistsException("ebuser.not.exists");
        }
        String codeKey = CacheConstants.LOGIN_CODE_KEY + phone;
        // 生成code
        String code = RandomUtil.randomNumbers(4);
        // 缓存
        redisCache.setCacheObject(codeKey, code, CacheConstants.LOGIN_CODE_EXPIRATION, TimeUnit.MINUTES);

        // 腾讯云发送短信
        try {
            sendMsg(phone, code);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Error sending SMS message to {}, {}", phone, e.getMessage());
            redisCache.deleteObject(codeKey);
            return error("短信发送失败");
        }
        return success();
    }

    private void sendMsg(String phone, String code) throws HTTPException, IOException {
        SmsSingleSender smsSingleSender = new SmsSingleSender(1400806003, "4d151d582e17ee85d5fd11e2fdb6394d");
        SmsSingleSenderResult result = smsSingleSender.sendWithParam("86", phone, 820413, new String[]{code}, "大唐泰信", "", "");
        System.out.println(result);
    }
}
