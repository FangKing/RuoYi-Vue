package com.ruoyi.web.controller.system;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.EbPrplregist;
import com.ruoyi.system.domain.EbPrplregistDistribution;
import com.ruoyi.system.service.IEbPrplregistDistributionService;
import com.ruoyi.system.service.IEbPrplregistService;
import com.ruoyi.system.service.ISysRoleService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

/**
 * 报案单Controller
 * 
 * @author ruoyi
 * @date 2023-09-13
 */
@CrossOrigin
@RestController
@RequestMapping("/system/prplregist")
public class EbPrplregistController extends BaseController
{
    private String prefix = "system/prplregist";

    @Autowired
    private IEbPrplregistService ebPrplregistService;
    @Autowired
    private IEbPrplregistDistributionService distributionService;
    @Autowired
    private ISysUserService userService;
    @Resource
    private ISysRoleService roleService;

    /**
     * 查询报案单列表
     */
    @GetMapping("/list")
    @ResponseBody
    public TableDataInfo list(EbPrplregist ebPrplregist)
    {
        Long userId = SecurityUtils.getUserId();
        Set<String> roleKeys = roleService.selectRolePermissionByUserId(userId);

        // 如果是分发人，则查看公司相关的数据
        // 否则只查看分配或者绑定的数据
        if (roleKeys.contains("bsgl")) {
            SysUser sysUser = userService.selectUserById(userId);
            String company = sysUser.getCompany();

            ebPrplregist.setUserId(null);
            ebPrplregist.setIssueInsuranceCom(company);
        } else {
            ebPrplregist.setUserId(userId);
            ebPrplregist.setIssueInsuranceCom(null);
        }
        startPage();
        List<EbPrplregist> list = ebPrplregistService.selectEbPrplregistList(ebPrplregist);
        return getDataTable(list);
    }

    @PostMapping("/export")
    public void export(HttpServletResponse response, EbPrplregist ebPrplregist)
    {
        Long userId = SecurityUtils.getUserId();
        Set<String> roleKeys = roleService.selectRolePermissionByUserId(userId);

        // 如果是分发人，则查看公司相关的数据
        // 否则只查看分配或者绑定的数据
        if (roleKeys.contains("bsgl")) {
            SysUser sysUser = userService.selectUserById(userId);
            String company = sysUser.getCompany();

            ebPrplregist.setUserId(null);
            ebPrplregist.setIssueInsuranceCom(company);
        } else {
            ebPrplregist.setUserId(userId);
            ebPrplregist.setIssueInsuranceCom(null);
        }
        List<EbPrplregist> list = ebPrplregistService.selectEbPrplregistList(ebPrplregist);

        ExcelUtil<EbPrplregist> util = new ExcelUtil<>(EbPrplregist.class);
        util.exportExcel(response, list, "案件数据");
    }

    /**
     * 新增保存报案单
     */
    @Log(title = "报案单", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(EbPrplregist ebPrplregist)
    {
        return toAjax(ebPrplregistService.insertEbPrplregist(ebPrplregist));
    }

    @GetMapping("/{id}")
    @ResponseBody
    public AjaxResult get(@PathVariable("id") String id)
    {
        EbPrplregist ebPrplregist = ebPrplregistService.selectEbPrplregistById(id);
        return success(ebPrplregist);
    }

    @GetMapping("/binding/{id}")
    @ResponseBody
    public AjaxResult binding(@PathVariable("id") String id)
    {
        // 1. 通过code换openId
        //JSONObject jsonObject = WxUtils.oauthAndGetOpenId(code);
        //String openId = jsonObject.getString("openId");

        // 2. 查询user....
        Long userId = SecurityUtils.getUserId();
        SysUser sysUser = userService.selectUserById(userId);
        if (sysUser == null) {
            return error("用户未登录");
        }

        EbPrplregist ebPrplregist = ebPrplregistService.selectEbPrplregistById(id);
        if (ebPrplregist == null || ebPrplregist.getId() == null) {
            return error("报案单不存在");
        }

        distributionService.bindPrplregist(id, sysUser);
        return success("绑定成功");
    }

    @PostMapping("/distribution")
    public AjaxResult distribution(@RequestBody EbPrplregistDistribution params)
    {
        if (StringUtils.isAllBlank(params.getIds())) {
            return error("报案单ID不能为空");
        }

        if (params.getUserId() == 0 && StringUtils.isBlank(params.getUserIds())) {
            return error("被分配人ID不能为空");
        }

        if (StringUtils.isBlank(params.getUserIds())) {
            params.setUserIds(params.getUserId()+"");
        }

        distributionService.distribution(params.getUserIds(), params.getIds());

        return success();
    }

    /**
     * 修改保存报案单
     */
    @Log(title = "报案单", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@RequestBody EbPrplregist ebPrplregist)
    {
        return toAjax(ebPrplregistService.updateEbPrplregist(ebPrplregist));
    }

    /**
     * 删除报案单
     */
    @Log(title = "报案单", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(ebPrplregistService.deleteEbPrplregistByIds(ids));
    }
}
