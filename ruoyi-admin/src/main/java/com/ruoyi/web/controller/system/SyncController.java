package com.ruoyi.web.controller.system;


import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson2.JSONException;
import com.alibaba.fastjson2.JSONObject;
import com.alibaba.fastjson2.util.BeanUtils;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.AESUtils;
import com.ruoyi.system.domain.EbPrplregist;
import com.ruoyi.system.domain.EbPrplregistSync;
import com.ruoyi.system.service.IEbPrplregistService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/sync")
public class SyncController extends BaseController {

    @Resource
    private IEbPrplregistService ebPrplregistService;

    private static final String KEY = "4tqrbl0yg3fui0py";

    @Log(title = "报案单基础同步", businessType = BusinessType.INSERT)
    @PostMapping("/prplregist")
    public AjaxResult syncPrplregist(@RequestBody SyncBody body) {
        String requestId = body.getRequestId();
        if (StringUtils.isEmpty(requestId)) {
            return new AjaxResult(4000, "请求编码不能为空");
        }
        String accountNo = body.getAccountNo();
        if (!StringUtils.equals("JKWT_DT", accountNo)) {
            return new AjaxResult(4000, "账号不存在");
        }

        String data = body.getData();
        logger.debug("获取报案单同步数据为：{}", data);
        String decrypt = StringUtils.EMPTY;
        try {
            decrypt = AESUtils.decrypt(data, KEY);
            logger.debug("获取报案单同步数据解析结果：{}", decrypt);
        } catch (Exception e) {
            e.printStackTrace();
            logger.warn("获取报案单同步数据解析失败：{}", data);
            return new AjaxResult(4000, "报文解密失败", data);
        }

        try {
            EbPrplregistSync ebPrplregistSync = JSONObject.parseObject(decrypt, EbPrplregistSync.class);
            EbPrplregist ebPrplregist = new EbPrplregist();
            BeanUtil.copyProperties(ebPrplregistSync, ebPrplregist);
            ebPrplregist.setRegistNo(ebPrplregistSync.getRegistno());

            return ebPrplregistService.syncEbPrplregist(ebPrplregist);
        } catch (Exception e) {
            e.printStackTrace();
            return new AjaxResult(6000, e.getMessage());
        }

    }

    @GetMapping("/prplregist/{id}")
    public AjaxResult getPrplregist(@PathVariable("id") String id, @RequestParam(name = "beginTime", required = false) String beginTime
            , @RequestParam(name = "endTime", required = false) String endTime) {
        if (StringUtils.isBlank(id)) {
            return error("id不能为空");
        }
        EbPrplregist ebPrplregist = ebPrplregistService.syncEbPrplregistById(id, beginTime, endTime);
        return new AjaxResult(0, "操作成功",ebPrplregist);
    }
}
