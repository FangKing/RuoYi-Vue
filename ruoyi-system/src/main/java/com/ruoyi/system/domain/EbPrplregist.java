package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import com.ruoyi.common.core.domain.entity.SysUser;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 报案单对象 eb_prplregist
 * 
 * @author ruoyi
 * @date 2023-09-13
 */
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class EbPrplregist extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private String id;

    /** 原始ID */
    @Excel(name = "原始ID")
    private String sourceId;

    /** 出单保险公司 **/
    @Excel(name = "出单保险公司")
    private String issueInsuranceCom;

    /** 报案序号 */
    @Excel(name = "报案序号")
    private String registNo;

    /** 所属二级单位 */
//    @Excel(name = "所属二级单位")
    private String secondUnitCode;

    /** 成员单位编码 */
//    @Excel(name = "成员单位编码")
    private String unitCode;

    /** 成员单位名称 */
//    @Excel(name = "成员单位名称")
    private String unitName;

    /** 险种代码 */
//    @Excel(name = "险种代码")
    private String riskCode;

    /** 险种名称 */
    @Excel(name = "险种名称")
    private String riskName;

    /** 省份 */
//    @Excel(name = "省份")
    private String province;

    /** 保单号 */
    @Excel(name = "保单号")
    private String policyNo;

    /** 标段号 */
//    @Excel(name = "标段号")
    private String bidSectionNo;

    /** 保险起期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
//    @Excel(name = "保险起期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startDate;

    /** 保险止期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
//    @Excel(name = "保险止期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endDate;

    /** 被保险人名称 */
    @Excel(name = "被保险人名称")
    private String insuredName;

    /** 报案人 */
    @Excel(name = "报案人")
    private String reportPerson;

    /** 报案人电话 */
    @Excel(name = "报案人电话")
    private String reportPersonPhone;

    /** 报案时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "报案时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date reportTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date reportTimeBeginTime;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date reportTimeEndTime;

    /** 出险时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "出险时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date riskTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date riskTimeBeginTime;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date riskTimeEndTime;

    /** 出险地点 */
    @Excel(name = "出险地点")
    private String riskAddress;

    /** 出险单位 */
    @Excel(name = "出险单位")
    private String riskUnit;

    /** 出险原因 */
    @Excel(name = "出险原因")
    private String riskReasons;

    /** 报损金额 */
    @Excel(name = "报损金额")
    private BigDecimal reportLossAmount;

    /** 币别 */
    @Excel(name = "币别")
    private String currency;

    /** 受损标的 */
//    @Excel(name = "受损标的")
    private String damagedTarget;

    /** 出险过程 */
    @Excel(name = "出险过程")
    private String beDangerProcess;

    /** 是否发送短信   （1：是；0否） */
    private String sendMessage;

    /** 保司编号 */
    @Excel(name = "保司编号")
    private String supSimpleNameNo;

    /** 推荐公估公司 */
    private String recAppraisalCom;

    /** 推荐公估人 */
    private String recAppraisalPerson;

    /** 推荐公估人手机号 */
    private String recAppraisalPersonPhone;

    /** 到账金额 */
    @Excel(name = "到账金额")
    private BigDecimal arrivalAccountAmount;

    /** 到账时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "到账时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date arrivalAccountTime;

    /** 确认人 */
    @Excel(name = "确认人")
    private String confirmUser;

    /** 确认时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "确认时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date confirmTime;

    /** 案件状态(报案申请1，报案确认2，报案作废3，结案4) */
    @Excel(name = "案件状态")
    private String claimstate;

    /** 出险通知书地址 */
    @Excel(name = "出险通知书地址")
    private String lossAdvicePath;

    /** 插入时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "插入时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date insertTime;

    /** 操作人 */
    @Excel(name = "操作人")
    private String updatedBy;


    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "操作时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /** 创建人 */
//    @Excel(name = "创建人")
    private String createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    private String updatedCompany;

    // 可操作人列表
    private List<SysUser> users;

    private Long userId;

    // 1 已分配；2 未分配
    private Integer hadDistributed;

    private EbPrplregistPaid paid;

    private List<EbPrplregistPaid> compensation;

    private List<EbPrplregistFile> files;

    public EbPrplregistPaid getPaid() {
        return paid;
    }

    public void setPaid(EbPrplregistPaid paid) {
        this.paid = paid;
    }

    public List<EbPrplregistFile> getFiles() {
        return files;
    }

    public void setFiles(List<EbPrplregistFile> files) {
        this.files = files;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setRegistNo(String registNo)
    {
        this.registNo = registNo;
    }

    public String getSourceId() {
        return sourceId;
    }

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public String getRegistNo()
    {
        return registNo;
    }
    public void setSecondUnitCode(String secondUnitCode)
    {
        this.secondUnitCode = secondUnitCode;
    }

    public String getSecondUnitCode()
    {
        return secondUnitCode;
    }
    public void setUnitCode(String unitCode)
    {
        this.unitCode = unitCode;
    }

    public String getUnitCode()
    {
        return unitCode;
    }
    public void setUnitName(String unitName)
    {
        this.unitName = unitName;
    }

    public String getUnitName()
    {
        return unitName;
    }
    public void setRiskCode(String riskCode)
    {
        this.riskCode = riskCode;
    }

    public String getRiskCode()
    {
        return riskCode;
    }
    public void setRiskName(String riskName)
    {
        this.riskName = riskName;
    }

    public String getRiskName()
    {
        return riskName;
    }
    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getProvince()
    {
        return province;
    }
    public void setPolicyNo(String policyNo)
    {
        this.policyNo = policyNo;
    }

    public String getPolicyNo()
    {
        return policyNo;
    }
    public void setBidSectionNo(String bidSectionNo)
    {
        this.bidSectionNo = bidSectionNo;
    }

    public String getBidSectionNo()
    {
        return bidSectionNo;
    }
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getStartDate()
    {
        return startDate;
    }
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    public Date getEndDate()
    {
        return endDate;
    }
    public void setReportPerson(String reportPerson)
    {
        this.reportPerson = reportPerson;
    }

    public String getReportPerson()
    {
        return reportPerson;
    }
    public void setReportPersonPhone(String reportPersonPhone)
    {
        this.reportPersonPhone = reportPersonPhone;
    }

    public String getReportPersonPhone()
    {
        return reportPersonPhone;
    }
    public void setReportTime(Date reportTime)
    {
        this.reportTime = reportTime;
    }

    public Date getReportTime()
    {
        return reportTime;
    }
    public void setRiskTime(Date riskTime)
    {
        this.riskTime = riskTime;
    }

    public Date getRiskTime()
    {
        return riskTime;
    }
    public void setRiskAddress(String riskAddress)
    {
        this.riskAddress = riskAddress;
    }

    public String getRiskAddress()
    {
        return riskAddress;
    }
    public void setRiskUnit(String riskUnit)
    {
        this.riskUnit = riskUnit;
    }

    public String getRiskUnit()
    {
        return riskUnit;
    }
    public void setRiskReasons(String riskReasons)
    {
        this.riskReasons = riskReasons;
    }

    public String getRiskReasons()
    {
        return riskReasons;
    }
    public void setReportLossAmount(BigDecimal reportLossAmount)
    {
        this.reportLossAmount = reportLossAmount;
    }

    public BigDecimal getReportLossAmount()
    {
        return reportLossAmount;
    }
    public void setCurrency(String currency)
    {
        this.currency = currency;
    }

    public String getCurrency()
    {
        return currency;
    }
    public void setDamagedTarget(String damagedTarget)
    {
        this.damagedTarget = damagedTarget;
    }

    public String getDamagedTarget()
    {
        return damagedTarget;
    }
    public void setBeDangerProcess(String beDangerProcess)
    {
        this.beDangerProcess = beDangerProcess;
    }

    public String getBeDangerProcess()
    {
        return beDangerProcess;
    }
    public void setArrivalAccountAmount(BigDecimal arrivalAccountAmount)
    {
        this.arrivalAccountAmount = arrivalAccountAmount;
    }

    public BigDecimal getArrivalAccountAmount()
    {
        return arrivalAccountAmount;
    }
    public void setArrivalAccountTime(Date arrivalAccountTime)
    {
        this.arrivalAccountTime = arrivalAccountTime;
    }

    public Date getArrivalAccountTime()
    {
        return arrivalAccountTime;
    }
    public void setConfirmUser(String confirmUser)
    {
        this.confirmUser = confirmUser;
    }

    public String getConfirmUser()
    {
        return confirmUser;
    }
    public void setConfirmTime(Date confirmTime)
    {
        this.confirmTime = confirmTime;
    }

    public Date getConfirmTime()
    {
        return confirmTime;
    }
    public void setClaimstate(String claimstate)
    {
        this.claimstate = claimstate;
    }

    public String getClaimstate()
    {
        return claimstate;
    }
    public void setLossAdvicePath(String lossAdvicePath)
    {
        this.lossAdvicePath = lossAdvicePath;
    }

    public String getLossAdvicePath()
    {
        return lossAdvicePath;
    }
    public void setUpdatedBy(String updatedBy)
    {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedBy()
    {
        return updatedBy;
    }
    public void setInsertTime(Date insertTime)
    {
        this.insertTime = insertTime;
    }

    public Date getInsertTime()
    {
        return insertTime;
    }
    public void setUpdatedTime(Date updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime()
    {
        return updatedTime;
    }
    public void setCreatedBy(String createdBy)
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy()
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime)
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime()
    {
        return createdTime;
    }
    public void setInsuredName(String insuredName)
    {
        this.insuredName = insuredName;
    }

    public String getInsuredName()
    {
        return insuredName;
    }

    public List<EbPrplregistPaid> getCompensation() {
        return compensation;
    }

    public void setCompensation(List<EbPrplregistPaid> compensation) {
        this.compensation = compensation;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<SysUser> getUsers() {
        return users;
    }

    public void setUsers(List<SysUser> users) {
        this.users = users;
    }

    public Integer getHadDistributed() {
        return hadDistributed;
    }

    public void setHadDistributed(Integer hadDistributed) {
        this.hadDistributed = hadDistributed;
    }

    public String getUpdatedCompany() {
        return updatedCompany;
    }

    public void setUpdatedCompany(String updatedCompany) {
        this.updatedCompany = updatedCompany;
    }

    public Date getReportTimeBeginTime() {
        return reportTimeBeginTime;
    }

    public void setReportTimeBeginTime(Date reportTimeBeginTime) {
        this.reportTimeBeginTime = reportTimeBeginTime;
    }

    public Date getReportTimeEndTime() {
        return reportTimeEndTime;
    }

    public void setReportTimeEndTime(Date reportTimeEndTime) {
        this.reportTimeEndTime = reportTimeEndTime;
    }

    public String getIssueInsuranceCom() {
        return issueInsuranceCom;
    }

    public void setIssueInsuranceCom(String issueInsuranceCom) {
        this.issueInsuranceCom = issueInsuranceCom;
    }

    public String getSendMessage() {
        return sendMessage;
    }

    public void setSendMessage(String sendMessage) {
        this.sendMessage = sendMessage;
    }

    public String getSupSimpleNameNo() {
        return supSimpleNameNo;
    }

    public void setSupSimpleNameNo(String supSimpleNameNo) {
        this.supSimpleNameNo = supSimpleNameNo;
    }

    public String getRecAppraisalCom() {
        return recAppraisalCom;
    }

    public void setRecAppraisalCom(String recAppraisalCom) {
        this.recAppraisalCom = recAppraisalCom;
    }

    public String getRecAppraisalPerson() {
        return recAppraisalPerson;
    }

    public void setRecAppraisalPerson(String recAppraisalPerson) {
        this.recAppraisalPerson = recAppraisalPerson;
    }

    public String getRecAppraisalPersonPhone() {
        return recAppraisalPersonPhone;
    }

    public void setRecAppraisalPersonPhone(String recAppraisalPersonPhone) {
        this.recAppraisalPersonPhone = recAppraisalPersonPhone;
    }

    public Date getRiskTimeBeginTime() {
        return riskTimeBeginTime;
    }

    public void setRiskTimeBeginTime(Date riskTimeBeginTime) {
        this.riskTimeBeginTime = riskTimeBeginTime;
    }

    public Date getRiskTimeEndTime() {
        return riskTimeEndTime;
    }

    public void setRiskTimeEndTime(Date riskTimeEndTime) {
        this.riskTimeEndTime = riskTimeEndTime;
    }

    @Override
    public String toString() {
        return "EbPrplregist{" +
                "id='" + id + '\'' +
                ", sourceId='" + sourceId + '\'' +
                ", issueInsuranceCom='" + issueInsuranceCom + '\'' +
                ", registNo='" + registNo + '\'' +
                ", secondUnitCode='" + secondUnitCode + '\'' +
                ", unitCode='" + unitCode + '\'' +
                ", unitName='" + unitName + '\'' +
                ", riskCode='" + riskCode + '\'' +
                ", riskName='" + riskName + '\'' +
                ", province='" + province + '\'' +
                ", policyNo='" + policyNo + '\'' +
                ", bidSectionNo='" + bidSectionNo + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", reportPerson='" + reportPerson + '\'' +
                ", reportPersonPhone='" + reportPersonPhone + '\'' +
                ", reportTime=" + reportTime +
                ", reportTimeBeginTime=" + reportTimeBeginTime +
                ", reportTimeEndTime=" + reportTimeEndTime +
                ", riskTime=" + riskTime +
                ", riskAddress='" + riskAddress + '\'' +
                ", riskUnit='" + riskUnit + '\'' +
                ", riskReasons='" + riskReasons + '\'' +
                ", reportLossAmount=" + reportLossAmount +
                ", currency='" + currency + '\'' +
                ", damagedTarget='" + damagedTarget + '\'' +
                ", beDangerProcess='" + beDangerProcess + '\'' +
                ", sendMessage='" + sendMessage + '\'' +
                ", supSimpleNameNo='" + supSimpleNameNo + '\'' +
                ", recAppraisalCom='" + recAppraisalCom + '\'' +
                ", recAppraisalPerson='" + recAppraisalPerson + '\'' +
                ", recAppraisalPersonPhone='" + recAppraisalPersonPhone + '\'' +
                ", arrivalAccountAmount=" + arrivalAccountAmount +
                ", arrivalAccountTime=" + arrivalAccountTime +
                ", confirmUser='" + confirmUser + '\'' +
                ", confirmTime=" + confirmTime +
                ", claimstate='" + claimstate + '\'' +
                ", lossAdvicePath='" + lossAdvicePath + '\'' +
                ", updatedBy='" + updatedBy + '\'' +
                ", insertTime=" + insertTime +
                ", updatedTime=" + updatedTime +
                ", createdBy='" + createdBy + '\'' +
                ", createdTime=" + createdTime +
                ", insuredName='" + insuredName + '\'' +
                ", updatedCompany='" + updatedCompany + '\'' +
                ", users=" + users +
                ", userId=" + userId +
                ", hadDistributed=" + hadDistributed +
                ", paid=" + paid +
                ", compensation=" + compensation +
                ", files=" + files +
                '}';
    }
}
