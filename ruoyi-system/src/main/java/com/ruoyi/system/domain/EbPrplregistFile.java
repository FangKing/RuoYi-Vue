package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 报案附件对象 eb_prplregist_file
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class EbPrplregistFile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 报案单ID */
    @Excel(name = "报案单ID")
    private String prplregistId;

    /** 赔付单ID */
    @Excel(name = "赔付单ID")
    private String prplregistPaidId;

    /** 文件名 */
    @Excel(name = "文件名")
    private String fileName;

    /** 原始文件名 */
    @Excel(name = "原始文件名")
    private String oriFileName;

    /** 文件后缀名 */
    @Excel(name = "文件后缀名")
    private String fileExt;

    /** 文件路径 */
    @Excel(name = "文件路径")
    private String filePath;

    /** 文件大小 */
    @Excel(name = "文件大小")
    private String fileSize;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setPrplregistId(String prplregistId)
    {
        this.prplregistId = prplregistId;
    }

    public String getPrplregistId()
    {
        return prplregistId;
    }
    public void setPrplregistPaidId(String prplregistPaidId)
    {
        this.prplregistPaidId = prplregistPaidId;
    }

    public String getPrplregistPaidId()
    {
        return prplregistPaidId;
    }
    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getFileName()
    {
        return fileName;
    }
    public void setOriFileName(String oriFileName)
    {
        this.oriFileName = oriFileName;
    }

    public String getOriFileName()
    {
        return oriFileName;
    }
    public void setFileExt(String fileExt)
    {
        this.fileExt = fileExt;
    }

    public String getFileExt()
    {
        return fileExt;
    }
    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public String getFilePath()
    {
        return filePath;
    }
    public void setFileSize(String fileSize)
    {
        this.fileSize = fileSize;
    }

    public String getFileSize()
    {
        return fileSize;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getStatus()
    {
        return status;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("prplregistId", getPrplregistId())
            .append("prplregistPaidId", getPrplregistPaidId())
            .append("fileName", getFileName())
            .append("oriFileName", getOriFileName())
            .append("fileExt", getFileExt())
            .append("filePath", getFilePath())
            .append("fileSize", getFileSize())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
