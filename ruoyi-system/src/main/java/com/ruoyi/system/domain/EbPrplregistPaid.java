package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 赔付单对象 eb_prplregist_paid
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class EbPrplregistPaid extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private String id;

    /** 报案单ID */
    @Excel(name = "报案单ID")
    private String prplregistId;

    /** 赔案号 */
    @Excel(name = "赔案号")
    private String paidNo;

    /** 赔付状态 */
    @Excel(name = "赔付状态")
    private String paidState;

    private String paidStateCode;

    /** 赔付金额 */
    @Excel(name = "赔付金额")
    private BigDecimal paidAmount;

    /** 进展说明 */
    @Excel(name = "进展说明")
    private String paidComments;

    private String reporterName;

    private String reporterPhone;

    private String reporterCom;
    /** 保司公估公司 */
    private String appraisalCom;
    /** 保司公估人 */
    private String appraisalPerson;
    /** 保司公估人手机号 */
    private String appraisalPersonPhone;

    private Date beginTime;

    private Date endTime;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    private List<EbPrplregistFile> files;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setPrplregistId(String prplregistId)
    {
        this.prplregistId = prplregistId;
    }

    public String getPrplregistId()
    {
        return prplregistId;
    }
    public void setPaidNo(String paidNo)
    {
        this.paidNo = paidNo;
    }

    public String getPaidNo()
    {
        return paidNo;
    }
    public void setPaidState(String paidState)
    {
        this.paidState = paidState;
    }

    public String getPaidState()
    {
        return paidState;
    }
    public void setPaidComments(String paidComments)
    {
        this.paidComments = paidComments;
    }

    public String getPaidComments()
    {
        return paidComments;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public List<EbPrplregistFile> getFiles() {
        return files;
    }

    public void setFiles(List<EbPrplregistFile> files) {
        this.files = files;
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getReporterName() {
        return reporterName;
    }

    public void setReporterName(String reporterName) {
        this.reporterName = reporterName;
    }

    public String getReporterPhone() {
        return reporterPhone;
    }

    public void setReporterPhone(String reporterPhone) {
        this.reporterPhone = reporterPhone;
    }

    public String getReporterCom() {
        return reporterCom;
    }

    public void setReporterCom(String reporterCom) {
        this.reporterCom = reporterCom;
    }

    public String getPaidStateCode() {
        return paidStateCode;
    }

    public void setPaidStateCode(String paidStateCode) {
        this.paidStateCode = paidStateCode;
    }

    public String getAppraisalCom() {
        return appraisalCom;
    }

    public void setAppraisalCom(String appraisalCom) {
        this.appraisalCom = appraisalCom;
    }

    public String getAppraisalPerson() {
        return appraisalPerson;
    }

    public void setAppraisalPerson(String appraisalPerson) {
        this.appraisalPerson = appraisalPerson;
    }

    public String getAppraisalPersonPhone() {
        return appraisalPersonPhone;
    }

    public void setAppraisalPersonPhone(String appraisalPersonPhone) {
        this.appraisalPersonPhone = appraisalPersonPhone;
    }

    @Override
    public String toString() {
        return "EbPrplregistPaid{" +
                "id='" + id + '\'' +
                ", prplregistId='" + prplregistId + '\'' +
                ", paidNo='" + paidNo + '\'' +
                ", paidState='" + paidState + '\'' +
                ", paidStateCode='" + paidStateCode + '\'' +
                ", paidAmount=" + paidAmount +
                ", paidComments='" + paidComments + '\'' +
                ", reporterName='" + reporterName + '\'' +
                ", reporterPhone='" + reporterPhone + '\'' +
                ", reporterCom='" + reporterCom + '\'' +
                ", appraisalCom='" + appraisalCom + '\'' +
                ", appraisalPerson='" + appraisalPerson + '\'' +
                ", appraisalPersonPhone='" + appraisalPersonPhone + '\'' +
                ", beginTime=" + beginTime +
                ", endTime=" + endTime +
                ", delFlag='" + delFlag + '\'' +
                ", files=" + files +
                '}';
    }
}
