package com.ruoyi.system.mapper;


import com.ruoyi.system.domain.EbPrplregist;

import java.util.List;


/**
 * 报案单Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-13
 */
public interface EbPrplregistMapper 
{
    /**
     * 查询报案单
     * 
     * @param id 报案单主键
     * @return 报案单
     */
    public EbPrplregist selectEbPrplregistById(String id);

    EbPrplregist selectEbPrplregistBySourceId(String id);

    /**
     * 查询报案单列表
     * 
     * @param ebPrplregist 报案单
     * @return 报案单集合
     */
    public List<EbPrplregist> selectEbPrplregistList(EbPrplregist ebPrplregist);

    List<EbPrplregist> selectUnDistributeList(EbPrplregist ebPrplregist);
    /**
     * 新增报案单
     * 
     * @param ebPrplregist 报案单
     * @return 结果
     */
    public int insertEbPrplregist(EbPrplregist ebPrplregist);

    /**
     * 修改报案单
     * 
     * @param ebPrplregist 报案单
     * @return 结果
     */
    public int updateEbPrplregist(EbPrplregist ebPrplregist);

    /**
     * 删除报案单
     * 
     * @param id 报案单主键
     * @return 结果
     */
    public int deleteEbPrplregistById(String id);

    /**
     * 批量删除报案单
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEbPrplregistByIds(String[] ids);
}
