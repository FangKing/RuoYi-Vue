package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.EbUser;

public interface EbUserMapper {
    EbUser selectEbUserById(String id);
    EbUser selectEbUserByPhone(String phone);
}
