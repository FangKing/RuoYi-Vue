package com.ruoyi.system.mapper;


import com.ruoyi.system.domain.EbPrplregistFile;

import java.util.List;

/**
 * 报案附件Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
public interface EbPrplregistFileMapper 
{
    /**
     * 查询报案附件
     * 
     * @param id 报案附件主键
     * @return 报案附件
     */
    public EbPrplregistFile selectEbPrplregistFileById(String id);

    /**
     * 查询报案附件列表
     * 
     * @param ebPrplregistFile 报案附件
     * @return 报案附件集合
     */
    public List<EbPrplregistFile> selectEbPrplregistFileList(EbPrplregistFile ebPrplregistFile);

    /**
     * 新增报案附件
     * 
     * @param ebPrplregistFile 报案附件
     * @return 结果
     */
    public int insertEbPrplregistFile(EbPrplregistFile ebPrplregistFile);

    /**
     * 修改报案附件
     * 
     * @param ebPrplregistFile 报案附件
     * @return 结果
     */
    public int updateEbPrplregistFile(EbPrplregistFile ebPrplregistFile);

    /**
     * 删除报案附件
     * 
     * @param id 报案附件主键
     * @return 结果
     */
    public int deleteEbPrplregistFileById(String id);

    /**
     * 批量删除报案附件
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEbPrplregistFileByIds(String[] ids);
}
