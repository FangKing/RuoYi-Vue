package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.EbPrplregistDistribution;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 报案单分配Mapper接口
 * 
 * @author ruoyi
 * @date 2023-10-11
 */
public interface EbPrplregistDistributionMapper 
{
    /**
     * 查询报案单分配
     * 
     * @param id 报案单分配主键
     * @return 报案单分配
     */
    public EbPrplregistDistribution selectEbPrplregistDistributionById(Long id);

    /**
     * 查询报案单分配列表
     * 
     * @param ebPrplregistDistribution 报案单分配
     * @return 报案单分配集合
     */
    public List<EbPrplregistDistribution> selectEbPrplregistDistributionList(EbPrplregistDistribution ebPrplregistDistribution);

    /**
     * 新增报案单分配
     * 
     * @param ebPrplregistDistribution 报案单分配
     * @return 结果
     */
    public int insertEbPrplregistDistribution(EbPrplregistDistribution ebPrplregistDistribution);

    /**
     * 修改报案单分配
     * 
     * @param ebPrplregistDistribution 报案单分配
     * @return 结果
     */
    public int updateEbPrplregistDistribution(EbPrplregistDistribution ebPrplregistDistribution);

    /**
     * 删除报案单分配
     * 
     * @param id 报案单分配主键
     * @return 结果
     */
    public int deleteEbPrplregistDistributionById(Long id);

    /**
     * 批量删除报案单分配
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEbPrplregistDistributionByIds(String[] ids);

    void syncPrplregistDistributionUser(@Param("openId") String openId, @Param("userId") Long userId);
}
