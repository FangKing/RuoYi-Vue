package com.ruoyi.system.service;


import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.domain.EbPrplregist;

import java.util.List;

/**
 * 报案单Service接口
 * 
 * @author ruoyi
 * @date 2023-09-13
 */
public interface IEbPrplregistService 
{
    /**
     * 查询报案单
     * 
     * @param id 报案单主键
     * @return 报案单
     */
    EbPrplregist selectEbPrplregistById(String id);

    EbPrplregist syncEbPrplregistById(String id, String beginDate, String endDate);

    /**
     * 查询报案单列表
     * 
     * @param ebPrplregist 报案单
     * @return 报案单集合
     */
    List<EbPrplregist> selectEbPrplregistList(EbPrplregist ebPrplregist);

    /**
     * 新增报案单
     * 
     * @param ebPrplregist 报案单
     * @return 结果
     */
    int insertEbPrplregist(EbPrplregist ebPrplregist);

    /**
     * 修改报案单
     * 
     * @param ebPrplregist 报案单
     * @return 结果
     */
    int updateEbPrplregist(EbPrplregist ebPrplregist);

    /**
     * 批量删除报案单
     * 
     * @param ids 需要删除的报案单主键集合
     * @return 结果
     */
    int deleteEbPrplregistByIds(String ids);

    /**
     * 删除报案单信息
     * 
     * @param id 报案单主键
     * @return 结果
     */
    int deleteEbPrplregistById(String id);

    AjaxResult syncEbPrplregist(EbPrplregist ebPrplregist);

    /**
     * 自动分配
     */
    void distributeAuto();
}
