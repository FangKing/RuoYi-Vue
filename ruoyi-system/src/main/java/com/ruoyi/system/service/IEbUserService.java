package com.ruoyi.system.service;

import com.ruoyi.system.domain.EbUser;

public interface IEbUserService {
    EbUser selectEbUserById(String id);
    EbUser selectEbUserByPhone(String phone);
}
