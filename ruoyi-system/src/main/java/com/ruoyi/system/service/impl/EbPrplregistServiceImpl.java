package com.ruoyi.system.service.impl;

import cn.hutool.core.util.StrUtil;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.system.domain.EbPrplregist;
import com.ruoyi.system.domain.EbPrplregistDistribution;
import com.ruoyi.system.domain.EbPrplregistFile;
import com.ruoyi.system.domain.EbPrplregistPaid;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.IEbPrplregistDistributionService;
import com.ruoyi.system.service.IEbPrplregistService;
import com.ruoyi.system.service.ISysRoleService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 报案单Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-13
 */
@Service
public class EbPrplregistServiceImpl implements IEbPrplregistService
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource
    private EbPrplregistMapper ebPrplregistMapper;
    @Resource
    private EbPrplregistPaidMapper paidMapper;
    @Resource
    private EbPrplregistFileMapper fileMapper;
    @Resource
    private IEbPrplregistDistributionService distributionService;
    @Resource
    private ISysUserService userService;
    @Resource
    private EbPrplregistDistributionMapper distributionMapper;
    @Resource
    private SysUserMapper userMapper;
    /**
     * 查询报案单
     * 
     * @param id 报案单主键
     * @return 报案单
     */
    @Override
    public EbPrplregist selectEbPrplregistById(String id)
    {
        EbPrplregist ebPrplregist = ebPrplregistMapper.selectEbPrplregistById(id);

        if (ebPrplregist == null) {
            return new EbPrplregist();
        }

        // 赔付编辑
        EbPrplregistPaid ebPrplregistPaid = new EbPrplregistPaid();
        ebPrplregistPaid.setPrplregistId(ebPrplregist.getId());
        List<EbPrplregistPaid> ebPrplregistPaids = paidMapper.selectEbPrplregistPaidList(ebPrplregistPaid);

        // 相关附件
        EbPrplregistFile ebPrplregistFile = new EbPrplregistFile();
        ebPrplregistFile.setPrplregistId(ebPrplregist.getId());
        List<EbPrplregistFile> ebPrplregistFiles = fileMapper.selectEbPrplregistFileList(ebPrplregistFile);

        // 可查看人员
        EbPrplregistDistribution param = new EbPrplregistDistribution();
        param.setPrplregistId(id);
        // param.setType("binding");
        List<EbPrplregistDistribution> ebPrplregistDistributions = distributionService.selectEbPrplregistDistributionList(param);
        ArrayList<SysUser> users = new ArrayList<>();
        for (EbPrplregistDistribution ebPrplregistDistribution : ebPrplregistDistributions) {
            SysUser user = userService.selectUserById(ebPrplregistDistribution.getUserId());
            SysUser userDTO = new SysUser();
            userDTO.setUserName(user.getUserName());
            userDTO.setNickName(user.getNickName());
            userDTO.setCompany(user.getCompany());
            userDTO.setPhonenumber(user.getPhonenumber());
            userDTO.setUserId(user.getUserId());
            users.add(userDTO);
        }

        ebPrplregist.setUsers(users);
        ebPrplregist.setFiles(ebPrplregistFiles);
        ebPrplregist.setCompensation(ebPrplregistPaids);

        return ebPrplregist;
    }

    @Override
    public EbPrplregist syncEbPrplregistById(String id, String beginDate, String endDate) {
        EbPrplregist ebPrplregist = ebPrplregistMapper.selectEbPrplregistById(id);

        if (ebPrplregist == null) {
            return new EbPrplregist();
        }

        // 赔付编辑
        EbPrplregistPaid ebPrplregistPaid = new EbPrplregistPaid();
        ebPrplregistPaid.setPrplregistId(ebPrplregist.getId());
        ebPrplregistPaid.setBeginTime(DateUtils.parseDate(beginDate));
        ebPrplregistPaid.setEndTime(DateUtils.parseDate(endDate));
        List<EbPrplregistPaid> ebPrplregistPaids = paidMapper.selectEbPrplregistPaidList(ebPrplregistPaid);

        List<EbPrplregistPaid> paidResult = new ArrayList<>();

        for (EbPrplregistPaid prplregistPaid : ebPrplregistPaids) {

            EbPrplregistPaid paid = new EbPrplregistPaid();
            paid.setPaidNo(prplregistPaid.getPaidNo());
            paid.setPaidState(prplregistPaid.getPaidState());
            // 赔案状态代码
            paid.setPaidStateCode(prplregistPaid.getPaidStateCode());
            paid.setPaidComments(prplregistPaid.getPaidComments());
            paid.setReporterName(prplregistPaid.getReporterName());
            paid.setReporterPhone(prplregistPaid.getReporterPhone());
            paid.setReporterCom(prplregistPaid.getReporterCom());
            paid.setAppraisalCom(prplregistPaid.getAppraisalCom());
            paid.setAppraisalPerson(prplregistPaid.getAppraisalPerson());
            paid.setAppraisalPersonPhone(prplregistPaid.getAppraisalPersonPhone());
            paid.setUpdateTime(prplregistPaid.getUpdateTime());
            paid.setPaidAmount(prplregistPaid.getPaidAmount());

            EbPrplregistFile ebPrplregistFile = new EbPrplregistFile();
            ebPrplregistFile.setPrplregistPaidId(prplregistPaid.getId());
            List<EbPrplregistFile> ebPrplregistFiles = fileMapper.selectEbPrplregistFileList(ebPrplregistFile);

            List<EbPrplregistFile> fileResult = new ArrayList<>();
            for (EbPrplregistFile prplregistFile : ebPrplregistFiles) {
                EbPrplregistFile file = new EbPrplregistFile();
                file.setOriFileName(prplregistFile.getOriFileName());
                file.setFilePath(prplregistFile.getFilePath());
                fileResult.add(file);
            }

            paid.setFiles(fileResult);

            paidResult.add(paid);
        }
        EbPrplregist result = new EbPrplregist();
        result.setId(ebPrplregist.getId());
        result.setSourceId(ebPrplregist.getSourceId());

        result.setCompensation(paidResult);
        return result;
    }

    /**
     * 查询报案单列表
     * 
     * @param ebPrplregist 报案单
     * @return 报案单
     */
    @Override
    public List<EbPrplregist> selectEbPrplregistList(EbPrplregist ebPrplregist)
    {
        return ebPrplregistMapper.selectEbPrplregistList(ebPrplregist);
    }

    /**
     * 新增报案单
     * 
     * @param ebPrplregist 报案单
     * @return 结果
     */
    @Override
    public int insertEbPrplregist(EbPrplregist ebPrplregist)
    {
        ebPrplregist.setCreatedTime(new Date());
        return ebPrplregistMapper.insertEbPrplregist(ebPrplregist);
    }

    /**
     * 修改报案单
     * 
     * @param ebPrplregist 报案单
     * @return 结果
     */
    @Override
    public int updateEbPrplregist(EbPrplregist ebPrplregist)
    {
        String id = ebPrplregist.getId();

        EbPrplregistPaid paid = ebPrplregist.getPaid();
        List<EbPrplregistFile> files = ebPrplregist.getFiles();

        Date now = new Date();
        // 处理付款单
        paid.setId(UUID.getRandomId());
        paid.setPrplregistId(id);
        paid.setCreateTime(now);
        paid.setCreateBy(SecurityUtils.getUsername());
        paid.setUpdateTime(now);
        paid.setUpdateBy(SecurityUtils.getUsername());

        Long userId = SecurityUtils.getUserId();
        SysUser sysUser = userService.selectUserById(userId);
        paid.setReporterCom(sysUser.getCompany());
        paid.setReporterName(sysUser.getUserName());
        paid.setReporterPhone(sysUser.getPhonenumber());

        paidMapper.insertEbPrplregistPaid(paid);

        // 处理附件
        if (files != null && files.size() > 0) {
            for (EbPrplregistFile file : files) {
                file.setId(UUID.getRandomId());
                file.setPrplregistId(id);
                file.setPrplregistPaidId(paid.getId());
                file.setCreateTime(now);
                file.setCreateBy(SecurityUtils.getUsername());
                file.setUpdateTime(now);
                file.setUpdateBy(SecurityUtils.getUsername());

                fileMapper.insertEbPrplregistFile(file);
            }
        }

        ebPrplregist.setUpdatedTime(new Date());

        ebPrplregist.setUpdatedBy(sysUser.getNickName());
        ebPrplregist.setUpdatedCompany(sysUser.getCompany());
        ebPrplregist.setClaimstate(paid.getPaidState());
        return ebPrplregistMapper.updateEbPrplregist(ebPrplregist);
    }

    /**
     * 批量删除报案单
     * 
     * @param ids 需要删除的报案单主键
     * @return 结果
     */
    @Override
    public int deleteEbPrplregistByIds(String ids)
    {
        return ebPrplregistMapper.deleteEbPrplregistByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除报案单信息
     * 
     * @param id 报案单主键
     * @return 结果
     */
    @Override
    public int deleteEbPrplregistById(String id)
    {
        return ebPrplregistMapper.deleteEbPrplregistById(id);
    }

    @Override
    public AjaxResult syncEbPrplregist(EbPrplregist ebPrplregist) {
        // 1. 校验
        // 1.1 ID校验，不能为空，不能已经存在
        String sourceId = ebPrplregist.getSourceId();
        if (StringUtils.isBlank(sourceId)) {
            return new AjaxResult(6000, "sourceId不能为空");
        }
        EbPrplregist sourceIdResult = ebPrplregistMapper.selectEbPrplregistBySourceId(sourceId);
        if (sourceIdResult != null) {
            EbPrplregist result = new EbPrplregist();
            result.setId(sourceIdResult.getId());
            result.setSourceId(sourceIdResult.getSourceId());
            result.setRemark("数据已推送");
            return new AjaxResult(0, "操作成功", result);
        }

        // 1.2 部分业务处理，待处理

        // 2. 保存
        ebPrplregist.setId(UUID.getRandomId());
        ebPrplregist.setCreatedTime(new Date());
        int i = ebPrplregistMapper.insertEbPrplregist(ebPrplregist);
        EbPrplregist result = new EbPrplregist();
        result.setId(ebPrplregist.getId());
        result.setSourceId(ebPrplregist.getSourceId());
        return i > 0 ? new AjaxResult(0, "操作成功", result) : AjaxResult.error();
    }

    @Transactional
    @Override
    public void distributeAuto() {
        // 查询出用户
        SysUser sysUser = new SysUser();
        sysUser.setRoleId(100l);
        List<SysUser> sysUsers = userMapper.selectAllocatedList(sysUser);

        if (sysUser == null || sysUsers.size() == 0) {
            logger.debug("未查询到需要分配的用户");
            return;
        }

        for (SysUser user : sysUsers) {
            String company = user.getCompany();
            if (StrUtil.isBlank(user.getCompany()) || !company.contains("有限公司")) {
                continue;
            }
            EbPrplregist ebPrplregist = new EbPrplregist();
            ebPrplregist.setIssueInsuranceCom(company.substring(0, company.indexOf("有限公司")).concat("有限公司"));
            List<EbPrplregist> ebPrplregists = ebPrplregistMapper.selectUnDistributeList(ebPrplregist);

            for (EbPrplregist prplregist : ebPrplregists) {
                EbPrplregistDistribution distribution = new EbPrplregistDistribution();
                distribution.setPrplregistId(prplregist.getId());
                distribution.setUserId(user.getUserId());
                distribution.setType("auto");
                distribution.setOpenId(user.getOpenId());

                distribution.setStatus(UserConstants.NORMAL);
                distribution.setCreateTime(DateUtils.getNowDate());
                distribution.setCreateBy(sysUser.getUserName());
                distribution.setUpdateTime(DateUtils.getNowDate());
                distribution.setUpdateBy(sysUser.getUserName());

                distributionMapper.insertEbPrplregistDistribution(distribution);
            }
        }
    }
}
