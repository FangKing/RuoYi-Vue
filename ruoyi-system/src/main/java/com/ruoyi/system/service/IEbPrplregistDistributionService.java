package com.ruoyi.system.service;


import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.EbPrplregistDistribution;

import java.util.List;

/**
 * 报案单分配Service接口
 * 
 * @author ruoyi
 * @date 2023-10-11
 */
public interface IEbPrplregistDistributionService 
{
    /**
     * 查询报案单分配
     * 
     * @param id 报案单分配主键
     * @return 报案单分配
     */
    public EbPrplregistDistribution selectEbPrplregistDistributionById(Long id);

    /**
     * 查询报案单分配列表
     * 
     * @param ebPrplregistDistribution 报案单分配
     * @return 报案单分配集合
     */
    public List<EbPrplregistDistribution> selectEbPrplregistDistributionList(EbPrplregistDistribution ebPrplregistDistribution);

    /**
     * 新增报案单分配
     * 
     * @param ebPrplregistDistribution 报案单分配
     * @return 结果
     */
    public int insertEbPrplregistDistribution(EbPrplregistDistribution ebPrplregistDistribution);

    void distribution(String userIds, String ids);

    /**
     * 修改报案单分配
     * 
     * @param ebPrplregistDistribution 报案单分配
     * @return 结果
     */
    public int updateEbPrplregistDistribution(EbPrplregistDistribution ebPrplregistDistribution);

    /**
     * 批量删除报案单分配
     * 
     * @param ids 需要删除的报案单分配主键集合
     * @return 结果
     */
    public int deleteEbPrplregistDistributionByIds(String ids);

    /**
     * 删除报案单分配信息
     * 
     * @param id 报案单分配主键
     * @return 结果
     */
    public int deleteEbPrplregistDistributionById(Long id);

    void syncPrplregistDistributionUser(String openId, Long userId);

    void bindPrplregist(String id, SysUser sysUser);
}
