package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.EbPrplregistFile;
import com.ruoyi.system.mapper.EbPrplregistFileMapper;
import com.ruoyi.system.service.IEbPrplregistFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 报案附件Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
@Service
public class EbPrplregistFileServiceImpl implements IEbPrplregistFileService
{
    @Autowired
    private EbPrplregistFileMapper ebPrplregistFileMapper;

    /**
     * 查询报案附件
     * 
     * @param id 报案附件主键
     * @return 报案附件
     */
    @Override
    public EbPrplregistFile selectEbPrplregistFileById(String id)
    {
        return ebPrplregistFileMapper.selectEbPrplregistFileById(id);
    }

    /**
     * 查询报案附件列表
     * 
     * @param ebPrplregistFile 报案附件
     * @return 报案附件
     */
    @Override
    public List<EbPrplregistFile> selectEbPrplregistFileList(EbPrplregistFile ebPrplregistFile)
    {
        return ebPrplregistFileMapper.selectEbPrplregistFileList(ebPrplregistFile);
    }

    /**
     * 新增报案附件
     * 
     * @param ebPrplregistFile 报案附件
     * @return 结果
     */
    @Override
    public int insertEbPrplregistFile(EbPrplregistFile ebPrplregistFile)
    {
        ebPrplregistFile.setCreateTime(DateUtils.getNowDate());
        return ebPrplregistFileMapper.insertEbPrplregistFile(ebPrplregistFile);
    }

    /**
     * 修改报案附件
     * 
     * @param ebPrplregistFile 报案附件
     * @return 结果
     */
    @Override
    public int updateEbPrplregistFile(EbPrplregistFile ebPrplregistFile)
    {
        ebPrplregistFile.setUpdateTime(DateUtils.getNowDate());
        return ebPrplregistFileMapper.updateEbPrplregistFile(ebPrplregistFile);
    }

    /**
     * 批量删除报案附件
     * 
     * @param ids 需要删除的报案附件主键
     * @return 结果
     */
    @Override
    public int deleteEbPrplregistFileByIds(String ids)
    {
        return ebPrplregistFileMapper.deleteEbPrplregistFileByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除报案附件信息
     * 
     * @param id 报案附件主键
     * @return 结果
     */
    @Override
    public int deleteEbPrplregistFileById(String id)
    {
        return ebPrplregistFileMapper.deleteEbPrplregistFileById(id);
    }
}
