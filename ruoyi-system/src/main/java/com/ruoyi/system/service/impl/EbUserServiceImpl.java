package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.EbUser;
import com.ruoyi.system.mapper.EbUserMapper;
import com.ruoyi.system.service.IEbUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class EbUserServiceImpl implements IEbUserService {
    @Resource
    private EbUserMapper userMapper;

    @Override
    public EbUser selectEbUserById(String id) {
        return userMapper.selectEbUserById(id);
    }

    @Override
    public EbUser selectEbUserByPhone(String phone) {
        return userMapper.selectEbUserByPhone(phone);
    }
}
