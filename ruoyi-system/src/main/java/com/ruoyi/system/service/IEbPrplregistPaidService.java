package com.ruoyi.system.service;


import com.ruoyi.system.domain.EbPrplregistPaid;

import java.util.List;

/**
 * 赔付单Service接口
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
public interface IEbPrplregistPaidService 
{
    /**
     * 查询赔付单
     * 
     * @param id 赔付单主键
     * @return 赔付单
     */
    public EbPrplregistPaid selectEbPrplregistPaidById(String id);

    /**
     * 查询赔付单列表
     * 
     * @param ebPrplregistPaid 赔付单
     * @return 赔付单集合
     */
    public List<EbPrplregistPaid> selectEbPrplregistPaidList(EbPrplregistPaid ebPrplregistPaid);

    /**
     * 新增赔付单
     * 
     * @param ebPrplregistPaid 赔付单
     * @return 结果
     */
    public int insertEbPrplregistPaid(EbPrplregistPaid ebPrplregistPaid);

    /**
     * 修改赔付单
     * 
     * @param ebPrplregistPaid 赔付单
     * @return 结果
     */
    public int updateEbPrplregistPaid(EbPrplregistPaid ebPrplregistPaid);

    /**
     * 批量删除赔付单
     * 
     * @param ids 需要删除的赔付单主键集合
     * @return 结果
     */
    public int deleteEbPrplregistPaidByIds(String ids);

    /**
     * 删除赔付单信息
     * 
     * @param id 赔付单主键
     * @return 结果
     */
    public int deleteEbPrplregistPaidById(String id);
}
