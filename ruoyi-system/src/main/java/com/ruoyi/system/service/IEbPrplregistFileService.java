package com.ruoyi.system.service;


import com.ruoyi.system.domain.EbPrplregistFile;

import java.util.List;

/**
 * 报案附件Service接口
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
public interface IEbPrplregistFileService 
{
    /**
     * 查询报案附件
     * 
     * @param id 报案附件主键
     * @return 报案附件
     */
    public EbPrplregistFile selectEbPrplregistFileById(String id);

    /**
     * 查询报案附件列表
     * 
     * @param ebPrplregistFile 报案附件
     * @return 报案附件集合
     */
    public List<EbPrplregistFile> selectEbPrplregistFileList(EbPrplregistFile ebPrplregistFile);

    /**
     * 新增报案附件
     * 
     * @param ebPrplregistFile 报案附件
     * @return 结果
     */
    public int insertEbPrplregistFile(EbPrplregistFile ebPrplregistFile);

    /**
     * 修改报案附件
     * 
     * @param ebPrplregistFile 报案附件
     * @return 结果
     */
    public int updateEbPrplregistFile(EbPrplregistFile ebPrplregistFile);

    /**
     * 批量删除报案附件
     * 
     * @param ids 需要删除的报案附件主键集合
     * @return 结果
     */
    public int deleteEbPrplregistFileByIds(String ids);

    /**
     * 删除报案附件信息
     * 
     * @param id 报案附件主键
     * @return 结果
     */
    public int deleteEbPrplregistFileById(String id);
}
