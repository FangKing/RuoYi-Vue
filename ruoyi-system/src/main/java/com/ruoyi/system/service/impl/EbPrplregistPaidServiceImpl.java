package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.EbPrplregistPaid;
import com.ruoyi.system.mapper.EbPrplregistPaidMapper;
import com.ruoyi.system.service.IEbPrplregistPaidService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 赔付单Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
@Service
public class EbPrplregistPaidServiceImpl implements IEbPrplregistPaidService
{
    @Autowired
    private EbPrplregistPaidMapper ebPrplregistPaidMapper;

    /**
     * 查询赔付单
     * 
     * @param id 赔付单主键
     * @return 赔付单
     */
    @Override
    public EbPrplregistPaid selectEbPrplregistPaidById(String id)
    {
        return ebPrplregistPaidMapper.selectEbPrplregistPaidById(id);
    }

    /**
     * 查询赔付单列表
     * 
     * @param ebPrplregistPaid 赔付单
     * @return 赔付单
     */
    @Override
    public List<EbPrplregistPaid> selectEbPrplregistPaidList(EbPrplregistPaid ebPrplregistPaid)
    {
        return ebPrplregistPaidMapper.selectEbPrplregistPaidList(ebPrplregistPaid);
    }

    /**
     * 新增赔付单
     * 
     * @param ebPrplregistPaid 赔付单
     * @return 结果
     */
    @Override
    public int insertEbPrplregistPaid(EbPrplregistPaid ebPrplregistPaid)
    {
        ebPrplregistPaid.setCreateTime(DateUtils.getNowDate());
        return ebPrplregistPaidMapper.insertEbPrplregistPaid(ebPrplregistPaid);
    }

    /**
     * 修改赔付单
     * 
     * @param ebPrplregistPaid 赔付单
     * @return 结果
     */
    @Override
    public int updateEbPrplregistPaid(EbPrplregistPaid ebPrplregistPaid)
    {
        ebPrplregistPaid.setUpdateTime(DateUtils.getNowDate());
        return ebPrplregistPaidMapper.updateEbPrplregistPaid(ebPrplregistPaid);
    }

    /**
     * 批量删除赔付单
     * 
     * @param ids 需要删除的赔付单主键
     * @return 结果
     */
    @Override
    public int deleteEbPrplregistPaidByIds(String ids)
    {
        return ebPrplregistPaidMapper.deleteEbPrplregistPaidByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除赔付单信息
     * 
     * @param id 赔付单主键
     * @return 结果
     */
    @Override
    public int deleteEbPrplregistPaidById(String id)
    {
        return ebPrplregistPaidMapper.deleteEbPrplregistPaidById(id);
    }
}
