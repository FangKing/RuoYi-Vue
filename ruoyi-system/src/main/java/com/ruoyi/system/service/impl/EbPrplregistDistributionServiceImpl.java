package com.ruoyi.system.service.impl;

import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.EbPrplregistDistribution;
import com.ruoyi.system.mapper.EbPrplregistDistributionMapper;
import com.ruoyi.system.service.IEbPrplregistDistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 报案单分配Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-10-11
 */
@Service
public class EbPrplregistDistributionServiceImpl implements IEbPrplregistDistributionService
{
    @Autowired
    private EbPrplregistDistributionMapper distributionMapper;

    /**
     * 查询报案单分配
     * 
     * @param id 报案单分配主键
     * @return 报案单分配
     */
    @Override
    public EbPrplregistDistribution selectEbPrplregistDistributionById(Long id)
    {
        return distributionMapper.selectEbPrplregistDistributionById(id);
    }

    /**
     * 查询报案单分配列表
     * 
     * @param ebPrplregistDistribution 报案单分配
     * @return 报案单分配
     */
    @Override
    public List<EbPrplregistDistribution> selectEbPrplregistDistributionList(EbPrplregistDistribution ebPrplregistDistribution)
    {
        return distributionMapper.selectEbPrplregistDistributionList(ebPrplregistDistribution);
    }

    /**
     * 新增报案单分配
     * 
     * @param ebPrplregistDistribution 报案单分配
     * @return 结果
     */
    @Override
    public int insertEbPrplregistDistribution(EbPrplregistDistribution ebPrplregistDistribution)
    {
        ebPrplregistDistribution.setStatus(UserConstants.NORMAL);
        ebPrplregistDistribution.setCreateTime(DateUtils.getNowDate());
        //ebPrplregistDistribution.setCreateBy(ShiroUtils.getLoginName());
        ebPrplregistDistribution.setUpdateTime(DateUtils.getNowDate());
        //ebPrplregistDistribution.setUpdateBy(ShiroUtils.getLoginName());
        return distributionMapper.insertEbPrplregistDistribution(ebPrplregistDistribution);
    }

    @Override
    public void distribution(String userIds, String ids) {
        if (StringUtils.isAnyBlank(userIds, ids)) {
            return;
        }

        Long[] userIdArray = Convert.toLongArray(userIds);
        String[] idArray = Convert.toStrArray(ids);
        for (int j = 0; j < userIdArray.length; j++) {
            for (int i = 0; i < idArray.length; i++) {
                EbPrplregistDistribution distribution = new EbPrplregistDistribution();
                distribution.setPrplregistId(idArray[i]);
                distribution.setUserId(userIdArray[j]);
                distribution.setType("distribution");

                insertEbPrplregistDistribution(distribution);
            }
        }
    }

    /**
     * 修改报案单分配
     * 
     * @param ebPrplregistDistribution 报案单分配
     * @return 结果
     */
    @Override
    public int updateEbPrplregistDistribution(EbPrplregistDistribution ebPrplregistDistribution)
    {
        ebPrplregistDistribution.setUpdateTime(DateUtils.getNowDate());
        return distributionMapper.updateEbPrplregistDistribution(ebPrplregistDistribution);
    }

    /**
     * 批量删除报案单分配
     * 
     * @param ids 需要删除的报案单分配主键
     * @return 结果
     */
    @Override
    public int deleteEbPrplregistDistributionByIds(String ids)
    {
        return distributionMapper.deleteEbPrplregistDistributionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除报案单分配信息
     * 
     * @param id 报案单分配主键
     * @return 结果
     */
    @Override
    public int deleteEbPrplregistDistributionById(Long id)
    {
        return distributionMapper.deleteEbPrplregistDistributionById(id);
    }

    @Override
    public void syncPrplregistDistributionUser(String openId, Long userId) {
        distributionMapper.syncPrplregistDistributionUser(openId, userId);
    }

    @Override
    public void bindPrplregist(String id, SysUser sysUser) {
        EbPrplregistDistribution distribution = new EbPrplregistDistribution();
        distribution.setPrplregistId(id);
        distribution.setUserId(sysUser.getUserId());
        distribution.setType("binding");

        List<EbPrplregistDistribution> ebPrplregistDistributions = distributionMapper.selectEbPrplregistDistributionList(distribution);

        if (ebPrplregistDistributions == null || ebPrplregistDistributions.size() == 0) {
            distribution.setStatus(UserConstants.NORMAL);
            distribution.setCreateTime(DateUtils.getNowDate());
            distribution.setCreateBy(sysUser.getUserName());
            distribution.setUpdateTime(DateUtils.getNowDate());
            distribution.setUpdateBy(sysUser.getUserName());

            distributionMapper.insertEbPrplregistDistribution(distribution);
        }
    }
}
